//
//  ViewController.swift
//  Product
//
//  Created by Elluminati on 23/06/2020.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var productList = [DataClass]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        let cellSize = CGSize(width: (screenWidth - 2) / 2, height: screenHeight / 3)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical //.horizontal
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 1
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        
        for n in 1...5 {
            productList.append(DataClass(id: n.description, productName: "product\(n)", img: "https://homepages.cae.wisc.edu/~ece533/images/watch.png", desc: "A classic wall clock. Licensed from iStockPhoto. noun. The definition of a clock is a device for measuring and showing the time of day, or a decoration on the side of a sock or stocking coming up from the ankle.", price: Double(100 * n), qty: 0))
            
        }
        
        collectionView.reloadData()
    }
    
    @IBAction func btnAddToCartClicked(_ sender: UIButton) {
        
        coreDataModel.AddCartData(ID: productList[sender.tag].id, productName: productList[sender.tag].productName, price: productList[sender.tag].price, qty: 1)
        
    }
}

extension ViewController : UICollectionViewDelegate
{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let newView = self.storyboard?.instantiateViewController(identifier: "ProductDetailVC") as! ProductDetailVC
        
        newView.ProductDetail = [productList[indexPath.row]]
        
        self.navigationController?.pushViewController(newView, animated: true)
    }
}

extension ViewController : UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! CollectionViewCell
        
        myCell.lblProductName.text = productList[indexPath.row].productName
        
        myCell.btnAddToCart.tag = indexPath.row
        
        if let url = NSURL(string: productList[indexPath.row].img) {
            if let data = NSData(contentsOf: url as URL) {
                myCell.imgView.image = UIImage(data: data as Data)
            }
        }
        
        return myCell
    }
    
}

