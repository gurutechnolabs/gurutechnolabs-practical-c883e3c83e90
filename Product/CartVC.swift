//
//  CartVC.swift
//  Product
//
//  Created by Rao-Mac-3 on 25/06/20.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit
import CoreData

class CartVC: UIViewController {

    @IBOutlet weak var tblCartDetail: UITableView!
    
    var cartDetail = [DataClass]()
    
    @IBOutlet weak var btnTotalPrice: UIButton!
    
    var total = Double()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        coreDataModel.retrieveCartData { (result, err) in
            print(result)
            
            for item in (result as! NSArray)
            {
                self.cartDetail.append(DataClass(id: (item as! CartDetail).id!, productName: (item as! CartDetail).productName!, img: "", desc: "", price: (item as! CartDetail).price, qty: (item as! CartDetail).qty))
            }
            
            self.tblCartDetail.reloadData()
            
            print(err)
        }
        
        
        
        for itm in cartDetail {
            var temp = 0.0
            temp = itm.price * itm.qty
            total = total + temp
        }
        
        
        
        btnTotalPrice.setTitle("Total: \(total.description)", for: .normal)
    }
    
    
}


extension CartVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let myCell = tblCartDetail.dequeueReusableCell(withIdentifier: "CartTableViewCell") as! CartTableViewCell
        
        myCell.textLabel?.text = cartDetail[indexPath.row].productName
        
        myCell.detailTextLabel?.text = cartDetail[indexPath.row].price.description
    
        
        return myCell
    }
}
