//
//  DataClass.swift
//  Product
//
//  Created by Rao-Mac-3 on 25/06/20.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit

class DataClass
{
    var id : String
    var productName : String
    var img : String
    var desc : String
    var price : Double
    var qty : Double
    
    init(id : String, productName : String, img : String, desc : String, price : Double, qty : Double)
    {
        self.id = id
        self.productName = productName
        self.img = img
        self.desc = desc
        self.price = price
        self.qty = qty
    }
}
