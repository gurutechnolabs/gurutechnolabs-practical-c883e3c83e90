//
//  CollectionViewCell.swift
//  Product
//
//  Created by Rao-Mac-3 on 24/06/20.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var btnAddToCart: UIButton!
}
