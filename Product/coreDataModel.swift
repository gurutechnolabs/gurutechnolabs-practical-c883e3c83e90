//
//  coreDataModel.swift
//  Product
//
//  Created by Rao-Mac-3 on 25/06/20.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class coreDataModel: NSObject
{
    public static func AddCartData(ID: String, productName: String, price : Double, qty: Double)
    {
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        
        //Now let’s create an entity and new user records.
        let userEntity = NSEntityDescription.entity(forEntityName: "CartDetail", in: managedContext)!
        
        //final, we need to add some data to our newly created record for each keys using
        //here adding 5 data with loop
        
        let user = NSManagedObject(entity: userEntity, insertInto: managedContext)
        user.setValue(ID, forKeyPath: "id")
        user.setValue(productName, forKeyPath: "productName")
        user.setValue(price, forKeyPath: "price")
        user.setValue(qty, forKeyPath: "qty")
        
        //Now we have set all the values. The next step is to save them inside the Core Data
        
        do {
            try managedContext.save()
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    
    public static func retrieveCartData(completionHandler: @escaping (Any, Error?) -> Void)
    {
        //As we know that container is set up in the AppDelegates so we need to refer that container.
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        //We need to create a context from this container
        let managedContext = appDelegate.persistentContainer.viewContext
        
        //Prepare the request of type NSFetchRequest  for the entity
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CartDetail")
        
        do {
            let result = try managedContext.fetch(fetchRequest)
            
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "id") as? String ?? "")
            }
            completionHandler(result,nil)
            
        } catch {
            completionHandler([],error)
            print("Failed")
        }
    }
    
    
    public static func UpdateCartData(id:String,qty:Double)
    {
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "CartDetail", in: managedContext)
        let request = NSFetchRequest<NSFetchRequestResult>()
        request.entity = entity
        let predicate = NSPredicate(format: "(id = %@)", id)
        request.predicate = predicate
        do {
            let results =
                try managedContext.fetch(request)
            let objectUpdate = results[0] as! NSManagedObject
            objectUpdate.setValue(qty, forKey: "qty")
            
            do {
                try managedContext.save()
                
            }catch let error as NSError {
                print(error)
            }
        }
        catch let error as NSError {
            print(error)
        }
    }
}
