//
//  ProductDetailVC.swift
//  Product
//
//  Created by Rao-Mac-3 on 25/06/20.
//  Copyright © 2020 Balu Muchhal. All rights reserved.
//

import UIKit

class ProductDetailVC: UIViewController {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblDesc: UILabel!
    
    var ProductDetail = [DataClass]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblDesc.text = ProductDetail[0].desc

        if let url = NSURL(string: ProductDetail[0].img) {
            if let data = NSData(contentsOf: url as URL) {
                imgView.image = UIImage(data: data as Data)
            }
        }
    }

    @IBAction func btnAddToCartClicked(_ sender: UIButton) {
        coreDataModel.AddCartData(ID: ProductDetail[0].id, productName: ProductDetail[0].productName, price: ProductDetail[0].price, qty: 1)
    }
}
